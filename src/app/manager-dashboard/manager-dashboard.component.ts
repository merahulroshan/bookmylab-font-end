import {Component, OnInit} from '@angular/core';
import {LabBookingService} from "../services/labbooking.service";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-manager-dashboard',
  templateUrl: './manager-dashboard.component.html',
  styleUrls: ['./manager-dashboard.component.css']
})
export class ManagerDashboardComponent implements OnInit {

  bookingData = [];
  temp = {};
  updateDate={};

  constructor(private service: LabBookingService) {
  }

  ngOnInit() {
    this.service.getManagerBookingDetails('1')
      .subscribe(res => {
          console.log(res);
          this.bookingData = [];
          this.temp = res;

          let j = 0, l = 0;
          for (let i in this.temp) {
            j++;
          }

          for (let k in this.temp) {
            l++;
            if (l > j - 2) {
              break;
            }
            //console.log(this.temp[k]);
            this.bookingData.push(this.temp[k]);

          }
          console.log(JSON.parse(JSON.stringify(res)));
        },
        (err: HttpErrorResponse) => {
          console.log(err);
          console.log(err.error);
          console.log(err.name);
          console.log(err.message);
          console.log(err.status);
        },
        () => {
          console.log('complete');
        }
      );
  }

}
