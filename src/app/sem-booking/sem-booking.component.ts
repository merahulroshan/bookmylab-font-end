import {Component} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Sem} from "../model/sem";
import {LabBookingService} from "../services/labbooking.service";
import {HttpErrorResponse} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-sem-booking',
  templateUrl: './sem-booking.component.html',
  styleUrls: ['./sem-booking.component.css']
})

export class SemBookingComponent {
  loading: boolean = false;
  model = {};
  slotDetails: number[];
  dateBooked: string;
  title = "Scanning Electron Microscope Lab Reservation";

  constructor(private semBookingService: LabBookingService, private router: Router) {
    console.log('dateBooked:');
    console.log(this.dateBooked);
  }

  onSlotSelected(slot: number[]) {
    this.slotDetails = slot;
  }

  onDateSelected(date: string) {
    this.dateBooked = date;
  }

  onFormSubmit(semForm: NgForm) {
    this.loading = true;
    let isBooked: boolean = false;
    let bookingId: string;
    let amount: number = 500;

    let other_value: string = '';
    let other: string = semForm.controls['other'].value;
    if (other == 'true') {
      other_value = semForm.controls['other_value'].value;
    }

    let form = new Sem(
      this.dateBooked,
      this.slotDetails[0],
      this.slotDetails[1],
      semForm.controls['material'].value,
      semForm.controls['method_preparation'].value,
      semForm.controls['metallic'].value == true ? '1' : '0',
      semForm.controls['ceramic'].value == true ? '1' : '0',
      semForm.controls['polymer_rubber'].value == true ? '1' : '0',
      semForm.controls['semiconductor'].value == true ? '1' : '0',
      other_value,
      semForm.controls['conductive_coating_required'].value == "true" ? '1' : '0',
      semForm.controls['se'].value == true ? '1' : '0',
      semForm.controls['bse'].value == true ? '1' : '0',
      semForm.controls['qma'].value == true ? '1' : '0',
      semForm.controls['xem'].value == true ? '1' : '0',
      semForm.controls['ls'].value == true ? '1' : '0',
      semForm.controls['ascan'].value == true ? '1' : '0',
      semForm.controls['other_requirements'].value);

    this.semBookingService.createSemBooking(form).subscribe(res => {
        this.loading = false;
        console.log("starting");
        console.log(res);
        isBooked = JSON.parse(JSON.stringify(res)).success;
        bookingId = JSON.parse(JSON.stringify(res)).sem_lab.booking_id;
        console.log('booking id :' + bookingId);
      },
      (err: HttpErrorResponse) => {
        this.loading = false;
        console.log(err);
        console.log(err.error);
        console.log(err.name);
        console.log(err.message);
        console.log(err.status);
      },
      () => {
        this.loading = false;
        console.log('completed');
        if (isBooked) {
          console.log("booked success");
          this.router.navigate(['/payment', {amount: amount, booking_id: bookingId}]);
        }
      }
    );
  }
}

