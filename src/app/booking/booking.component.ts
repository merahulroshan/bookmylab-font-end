import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BsDatepickerConfig} from "ngx-bootstrap";
import {SlotbookingService} from "../services/slotbooking.service";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit {

  id;
  loading: boolean = false;
  // isBlocked: boolean[] = [true, false, false, true, false, false, false, true, true, false, false, false, true];
  // isBlocked: boolean[] = [false, false, false, false, false, false, false, false, false, false, false, false];
  isBlocked: string[];
  isSelected: string[];
  // slots: string[] = ['8 AM', '9 AM', '10 AM', '11 AM', '12 PM', '1 PM', '2 PM', '3 PM', '4 PM', '5 PM', '6 PM', '7 PM', '8 PM'];
  // slotsName: string[] = ['Slot 0', 'Slot 1', 'Slot 2', 'Slot 3', 'Slot 4', 'Slot 5', 'Slot 6', 'Slot 7', 'Slot 8', 'Slot 9', 'Slot 10', 'Slot 11'];
  slotsName: string[] = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'];
  tooltipData: string[] = ['8am - 9am', '9am - 10am', '10am - 11am', '11am - 12pm', '12pm - 1pm', '1pm - 2pm', '2pm - 3pm', '3pm - 4pm', '4pm - 5pm', '5pm - 6pm', '6pm - 7pm', '7pm - 8pm'];

  minDate: Date;
  maxDate: Date;
  bsConfig: Partial<BsDatepickerConfig>;
  colorTheme = 'theme-green';
  bsValue: Date;
  bookingDateString: string;
  isResponseSuccess: boolean = false;
  bookingDate;
  @Output() onDateSelected = new EventEmitter<string>();
  @Output() onSlotSelected = new EventEmitter<number[]>();
  @Input() labNo;
  start: number = -1;
  end: number = -1;
  last: number = -1;
  lab_no: string;

  ngOnInit() {
    this.lab_no = this.labNo;
    console.log('labNo=' + this.lab_no);
  }

  constructor(private slotBookingService: SlotbookingService) {
    this.isBlocked = ['0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'];
    this.isSelected = ['0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'];
    // this.isSelected = [false, false, false, false, false, false, false, false, false, false, false, false];
    this.minDate = new Date();
    this.bsValue = new Date();
    this.maxDate = new Date();
    this.bsValue.setDate(this.bsValue.getDate() + 1);
    this.minDate = this.bsValue;
    this.maxDate.setDate(this.maxDate.getDate() + 7);
    this.bsConfig = Object.assign({}, {containerClass: this.colorTheme});
  }

  handleSelected(current: number) {
    if (this.start == -1) {
      this.start = current
      // console.log('log1');
    }

    if (this.end == -1) {
      this.end = current;
      // console.log('log2');
    }

    if (current > this.end) {
      if (!this.checkForBlocked(this.last, current)) {
        this.start = this.last;
        this.end = current;
        // console.log('log3');
      }
      else {
        this.end = this.start = current;
        // console.log('log4');
      }
    } else if (current < this.start) {
      if (!this.checkForBlocked(current, this.last)) {
        this.start = current;
        this.end = this.last;
        // console.log('log5');
      }
      else {
        this.end = this.start = current;
        // console.log('log6');
      }
    } else if (current > this.start && current < this.end) {
      if (this.last < current) {
        if (!this.checkForBlocked(this.last, current)) {
          this.start = this.last;
          this.end = current;
          // console.log('log7');
        }
        else {
          this.end = this.start = current;
          // console.log('log8');
        }
      } else {
        if (!this.checkForBlocked(current, this.last)) {
          this.start = current;
          this.end = this.last;
          // console.log('log9');
        }
        else {
          this.end = this.start = current;
          // console.log('log10');
        }
      }
    } else if (current == this.start || current == this.end) {
      this.start = this.end = current;
    }

    this.last = current;
    // console.log('log11');
  }

  checkForBlocked(start: number, end: number): number {
    let i: number;
    for (i = start + 1; i < end; i++) {
      if (this.isBlocked[i] == '1')
        return 1;
    }
    return 0;
  }

  toggleSelected(id: number) {
    this.id = id;
    let i: number;
    this.handleSelected(id);
    this.isSelected = ['0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'];
    for (i = this.start; i <= this.end; i++) {
      this.isSelected[i] = '1';
    }
    this.onSlotSelected.emit([this.start + 1, this.end + 1]);
  }

  initSlots(slots: Slots) {
    this.isBlocked[0] = slots.s1;
    this.isBlocked[1] = slots.s2;
    this.isBlocked[2] = slots.s3;
    this.isBlocked[3] = slots.s4;
    this.isBlocked[4] = slots.s5;
    this.isBlocked[5] = slots.s6;
    this.isBlocked[6] = slots.s7;
    this.isBlocked[7] = slots.s8;
    this.isBlocked[8] = slots.s9;
    this.isBlocked[9] = slots.s10;
    this.isBlocked[10] = slots.s11;
    this.isBlocked[11] = slots.s12;
    this.isBlocked[12] = slots.s13;
    this.isBlocked[13] = slots.s14;
    this.isBlocked[14] = slots.s15;
    this.isBlocked[15] = slots.s16;
    this.isBlocked[16] = slots.s17;
    this.isBlocked[17] = slots.s18;
    this.isBlocked[18] = slots.s19;
    this.isBlocked[19] = slots.s20;
    this.isBlocked[20] = slots.s21;
    this.isBlocked[21] = slots.s22;
    this.isBlocked[22] = slots.s23;
    this.isBlocked[23] = slots.s24;
    this.isSelected = ['0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'];
  }

  valueChange(newDate) {
    let isSuccess: boolean = false;
    this.loading = true;

    // hide slot timings
    this.isResponseSuccess = false;

    this.bookingDateString = newDate.toISOString().slice(0, 10).replace(/-/g, "-");
    let date = newDate.getDate();
    let month = newDate.getMonth() + 1;
    let year = newDate.getFullYear();

    this.bookingDate = year + '-' + month + '-' + date;

    // pass this date to parent component
    this.onDateSelected.emit(this.bookingDate);

    // fetching slot details for some date
    this.slotBookingService.getSlots(this.bookingDate, this.lab_no)
      .subscribe(
        res => {
          console.log("SlotBookingService started");
          console.log(res);

          this.initSlots(res);
          isSuccess = JSON.parse(JSON.stringify(res)).success;
          // show slot timings
        },
        (err: HttpErrorResponse) => {
          this.loading = false;
          console.log(err);
          console.log(err.error);
          console.log(err.name);
          console.log(err.message);
          console.log(err.status);
        },
        () => {
          this.loading = false;
          console.log('SlotBookingService completed');
          if (isSuccess) {
            this.isResponseSuccess = true;
          }
        }
      );
  }
}













