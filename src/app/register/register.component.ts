import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../user.service';
import {HttpErrorResponse} from '@angular/common/http';
import {User} from '../model/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerData: any = {};
  isUserExist = false;
  user;
  returnUrl: string;
  loading = false;

  constructor(private userService: UserService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  register(userData: NgForm) {
    this.loading = true;
    let user = new User(userData.controls['firstname'].value,
      "",
      userData.controls['lastname'].value,
      userData.controls['gender'].value,
      userData.controls['email'].value,
      userData.controls['password'].value);

    console.log("User = " + JSON.stringify(user));

    this.userService.create(user)
      .subscribe(res => {
          this.loading = false;
          console.log("started");
          console.log(res);
          this.user = JSON.stringify(res);
        },
        (err: HttpErrorResponse) => {
          this.loading = false;
          console.log(err);
          console.log(err.error);
          console.log(err.name);
          console.log(err.message);
          console.log(err.status);
        },
        () => {
          this.loading = false;
          console.log('complete');
          let user = JSON.parse(this.user);
          if (user.success) {
            this.router.navigate(['/verify']);
          } else {
            if (user.exist && !user.verified) {
              this.router.navigate(['/verify']);
            }
          }
        }
      );
  }
}
