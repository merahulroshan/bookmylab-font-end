import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
import {NgForm} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  ngOnInit(): void {
    //throw new Error('Method not implemented.');
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  loginData = {};
  user;
  returnUrl: string;
  loading = false;
  isLoginSuccess: boolean;

  constructor(private authService: AuthService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  login(userData: NgForm) {
    this.loading = true;
    let uid: string;
    let role_id: string;

    this.authService.login(userData.controls['username'].value, userData.controls['password'].value)
      .subscribe(res => {
          this.loading = false;
          console.log(res);
          this.isLoginSuccess = JSON.parse(JSON.stringify(res)).success;
          uid = JSON.parse(JSON.stringify(res)).user.user_id;
          role_id = JSON.parse(JSON.stringify(res)).user.role_id;
        },
        (err: HttpErrorResponse) => {
          this.loading = false;
          console.log(err);
          console.log(err.error);
          console.log(err.name);
          console.log(err.message);
          console.log(err.status);
        },
        () => {
          this.loading = false;
          console.log('complete');

          if (this.isLoginSuccess) {
            localStorage.setItem('currentUser', uid);
            localStorage.setItem('roleId', role_id);
            // console.log('userid@@' + uid);
            if (role_id == '4')
              this.router.navigate([this.returnUrl]);
            else if (role_id == '1' || role_id == '2' || role_id == '3') {
              this.router.navigate(['/manager']);
            }
          } else {
            alert('Wrong credentials');
          }
        }
      );
  }
}
