import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {PaymentService} from "../services/payment.service";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit, OnDestroy {
  bookingId: string;
  amount: string;
  loading: boolean = false;
  paymentSuccess: boolean;

  constructor(private route: ActivatedRoute, private paymentService: PaymentService, private router:Router) {
  }

  ngOnInit() {
    this.amount = this.route.snapshot.params['amount'];
    this.bookingId = this.route.snapshot.params['booking_id'];
  }

  ngOnDestroy(): void {
  }

  initiatePayment() {
    this.loading = true;
    this.paymentSuccess = false;
    this.paymentService.initiatePayment(this.amount, this.bookingId).subscribe(res => {
        console.log("PaymentService started");
        console.log(res);
        this.paymentSuccess = JSON.parse(JSON.stringify(res)).success;
      },
      (err: HttpErrorResponse) => {
        this.loading = false;
        console.log(err);
        console.log(err.error);
        console.log(err.name);
        console.log(err.message);
        console.log(err.status);
      },
      () => {
        console.log('PaymentService completed');
        this.loading = false;
        if(this.paymentSuccess){
          alert('payment successful');
          this.router.navigate(['/dashboard']);
        }
      }
    );
  }

}

