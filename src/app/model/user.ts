export class User {

  constructor(public firstname: string,
              public middlename: string,
              public lastname: string,
              public gender: string,
              public email: string,
              public password: string,) {
  }
}
