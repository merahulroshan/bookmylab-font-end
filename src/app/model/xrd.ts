export class Xrd {
  constructor(public booking_date: string = "",
              public slot_start: number = -1,
              public slot_end: number = -1,
              public material: string = '',
              public method_preparation: string = '',
              public scan_angle: string,
              public toxic: string = '0',
              public texture: string = '0',
              public residual_stress: string = '0',
              public saxs: string = '0',
              public material_type: string,
              public other_requirements: string = "") {
  }
}
