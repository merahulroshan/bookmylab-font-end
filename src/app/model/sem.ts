export class Sem {
  constructor(public booking_date: string = "",
              public slot_start: number = -1,
              public slot_end: number = -1,
              public material: string = '',
              public method_preparation: string = '',
              public metallic: string = '0',
              public ceramic: string = '0',
              public polymer_rubber: string = '0',
              public semiconductor: string = '0',
              public other_value: string = '',
              public conductive_coating_required: string = '0',
              public se: string = '0',
              public bse: string = '0',
              public qma: string = '0',
              public xem: string = '0',
              public ls: string = '0',
              public ascan: string = '0',
              public other_requirements: string = "") {
  }
}
