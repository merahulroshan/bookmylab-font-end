export class Spm {
  constructor(public booking_date: string = "",
              public slot_start: number = -1,
              public slot_end: number = -1,
              public material: string = '',
              public method_preparation: string = '',
              public toxic: string = '0',
              public conducting: string = '0',
              public material_type: string,
              public other_requirements: string = "") {
  }
}
