import {Component} from '@angular/core';
import {LabBookingService} from "../services/labbooking.service";
import {Router} from "@angular/router";
import {NgForm} from "@angular/forms";
import {Xrd} from "../model/xrd";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-xrd-booking',
  templateUrl: './xrd-booking.component.html',
  styleUrls: ['./xrd-booking.component.css']
})
export class XrdBookingComponent {
  loading: boolean = false;
  model = {};
  slotDetails: number[];
  dateBooked: string;
  title = "XRD Lab Reservation";

  constructor(private semBookingService: LabBookingService, private router: Router) {
    console.log('dateBooked:');
    console.log(this.dateBooked);
  }

  onSlotSelected(slot: number[]) {
    this.slotDetails = slot;
  }

  onDateSelected(date: string) {
    this.dateBooked = date;
  }

  onFormSubmit(semForm: NgForm) {
    let isBooked: boolean = false;
    let bookingId: string;
    let amount: number = 500;
    this.loading = true;

    let form = new Xrd(
      this.dateBooked,
      this.slotDetails[0],
      this.slotDetails[1],
      semForm.controls['material'].value,
      semForm.controls['method_preparation'].value,
      semForm.controls['scan_angle'].value,
      semForm.controls['toxic'].value == true ? '1' : '0',
      semForm.controls['texture'].value == true ? '1' : '0',
      semForm.controls['residual_stress'].value == true ? '1' : '0',
      semForm.controls['saxs'].value == true ? '1' : '0',
      semForm.controls['material_type'].value,
      semForm.controls['other_requirements'].value);

    this.semBookingService.createXrdBooking(form).subscribe(res => {
        this.loading = false;
        console.log("XRD booking starting");
        console.log(res);
        isBooked = JSON.parse(JSON.stringify(res)).success;
        bookingId = JSON.parse(JSON.stringify(res)).xrd_lab.booking_id;
        console.log('booking id :' + bookingId);
      },
      (err: HttpErrorResponse) => {
        this.loading = false;
        console.log(err);
        console.log(err.error);
        console.log(err.name);
        console.log(err.message);
        console.log(err.status);
      },
      () => {
        this.loading = false;
        console.log('XRD booking completed');
        if (isBooked) {
          console.log("booked success");
          this.router.navigate(['/payment', {amount: amount, booking_id: bookingId}]);
        }
      }
    );
  }
}
