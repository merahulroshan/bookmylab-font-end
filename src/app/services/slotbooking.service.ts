import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable()
export class SlotbookingService {
  url = 'https://iitrprlab.000webhostapp.com/getBookedSlots.php';

  constructor(private http: HttpClient) {
  }

  getSlots(date: string, lab_no: string) {
    let fd = new FormData();
    fd.append('date', date);
    fd.append('lab_number', lab_no);

    return this.http.post<Slots>(this.url, fd);
  }
}
