import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable()
export class PaymentService {
  url = 'https://iitrprlab.000webhostapp.com/payment.php';

  constructor(private http: HttpClient) {
  }

  initiatePayment(amount: string, bookingId: string) {
    let fd = new FormData();
    fd.append('money', amount);
    fd.append('booking_id', bookingId);

    return this.http.post(this.url, fd);
  }
}
