import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Sem} from "../model/sem";
import {Spm} from "../model/spm";
import {Xrd} from "../model/xrd";

@Injectable()
export class LabBookingService {
  url_sem = 'https://iitrprlab.000webhostapp.com/sem_booking.php';
  url_spm = 'https://iitrprlab.000webhostapp.com/spm_booking.php';
  url_xrd = 'https://iitrprlab.000webhostapp.com/xrd_booking.php';
  url_get_booking_details = 'https://iitrprlab.000webhostapp.com/viewMyBookings.php';
  url_get_manager_booking_details = 'https://iitrprlab.000webhostapp.com/manager_tasks.php';

  constructor(private http: HttpClient) {
  }

  createSemBooking(sem: Sem) {
    const fd = new FormData();
    fd.append('date', sem.booking_date);
    fd.append('slot_start', String(sem.slot_start));
    fd.append('slot_end', String(sem.slot_end));
    fd.append('user_id', localStorage.getItem('currentUser'));
    fd.append('material', sem.material);
    fd.append('prep_method', sem.method_preparation);
    fd.append('metallic', sem.metallic);
    fd.append('ceramic', sem.ceramic);
    fd.append('polymer_rubber', sem.polymer_rubber);
    fd.append('semiconductor', sem.semiconductor);
    fd.append('other', sem.other_value);
    fd.append('conductive_coating_required', sem.conductive_coating_required);
    fd.append('secondary_electron_detector', sem.se);
    fd.append('bse', sem.bse);
    fd.append('qma', sem.qma);
    fd.append('xem', sem.xem);
    fd.append('ls', sem.ls);
    fd.append('ascan', sem.ascan);
    fd.append('other_requirements', sem.other_requirements);

    return this.http.post(this.url_sem, fd);
  }

  createSpmBooking(spm: Spm) {
    const fd = new FormData();
    fd.append('date', spm.booking_date);
    fd.append('slot_start', String(spm.slot_start));
    fd.append('slot_end', String(spm.slot_end));
    fd.append('user_id', localStorage.getItem('currentUser'));
    fd.append('material', spm.material);
    fd.append('prep_method', spm.method_preparation);
    fd.append('toxic', spm.toxic);
    fd.append('conducting', spm.conducting);
    fd.append('material_type', spm.material_type);
    fd.append('other_requirements', spm.other_requirements);

    console.log('spm form data : ' + spm);
    return this.http.post(this.url_spm, fd);
  }

  createXrdBooking(xrd: Xrd) {
    const fd = new FormData();
    fd.append('date', xrd.booking_date);
    fd.append('slot_start', String(xrd.slot_start));
    fd.append('slot_end', String(xrd.slot_end));
    fd.append('user_id', localStorage.getItem('currentUser'));
    fd.append('material', xrd.material);
    fd.append('prep_method', xrd.method_preparation);
    fd.append('scan_angle', xrd.scan_angle);
    fd.append('toxic', xrd.toxic);
    fd.append('texture', xrd.texture);
    fd.append('residual_stress', xrd.residual_stress);
    fd.append('saxs', xrd.saxs);
    fd.append('material_type', xrd.material_type);
    fd.append('other_requirements', xrd.other_requirements);

    console.log('xrd form data : ' + xrd);
    return this.http.post(this.url_xrd, fd);
  }

  getBookingDetails(userId: string) {
    let fd = new FormData();
    fd.append('user_id', userId);
    console.log('userid:' + userId);
    return this.http.post(this.url_get_booking_details, fd);
  }

  getManagerBookingDetails(labNo: string) {
    let fd = new FormData();
    fd.append('code', '1');
    fd.append('lab_number', localStorage.getItem('roleId'));
    return this.http.post(this.url_get_manager_booking_details, fd);
  }

}

