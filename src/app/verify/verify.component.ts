import {Component, OnInit} from '@angular/core';
import {NgForm} from "@angular/forms";
import {UserService} from "../user.service";
import {HttpErrorResponse} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.css']
})
export class VerifyComponent {

  model = {};

  constructor(private userService: UserService, private router: Router) {
  }

  verify(data: NgForm) {
    let verified: boolean = false;
    this.userService.verify(data.controls['email'].value, data.controls['code'].value)
      .subscribe(res => {
          console.log(res);
          verified = JSON.parse(JSON.stringify(res)).verified;
        },
        (err: HttpErrorResponse) => {
          console.log(err);
          console.log(err.error);
          console.log(err.name);
          console.log(err.message);
          console.log(err.status);
        },
        () => {
          console.log('complete');
          if (verified) {
            alert('verified');
            this.router.navigate(['/dashboard']);
          }
        }
      );
  }

}
