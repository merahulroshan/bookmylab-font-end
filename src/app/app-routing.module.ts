import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {SemBookingComponent} from './sem-booking/sem-booking.component';
import {SpmBookingComponent} from './spm-booking/spm-booking.component';
import {XrdBookingComponent} from './xrd-booking/xrd-booking.component';
import {AuthGuard} from './auth.guard';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {ContactUsComponent} from './contact-us/contact-us.component';
import {MyAccountComponent} from './my-account/my-account.component';
import {BookingComponent} from "./booking/booking.component";
import {PaymentComponent} from "./payment/payment.component";
import {HeaderComponent} from "./header/header.component";
import {VerifyComponent} from "./verify/verify.component";
import {ManagerDashboardComponent} from "./manager-dashboard/manager-dashboard.component";

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
  {path: 'manager', component: ManagerDashboardComponent, canActivate: [AuthGuard]},
  {path: 'contact-us', component: ContactUsComponent},
  {path: 'my-account', component: MyAccountComponent, canActivate: [AuthGuard]},
  {path: 'sem-booking', component: SemBookingComponent, canActivate: [AuthGuard]},
  {path: 'spm-booking', component: SpmBookingComponent, canActivate: [AuthGuard]},
  {path: 'xrd-booking', component: XrdBookingComponent, canActivate: [AuthGuard]},
  {path: 'booking', component: BookingComponent, canActivate: [AuthGuard]},
  {path: 'verify', component: VerifyComponent},
  {path: 'payment', component: PaymentComponent},
  {path: '', component: DashboardComponent, canActivate: [AuthGuard]},
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
