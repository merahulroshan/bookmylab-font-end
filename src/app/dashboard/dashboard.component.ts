import {Component, OnInit} from '@angular/core';
import {HttpErrorResponse} from "@angular/common/http";
import {LabBookingService} from "../services/labbooking.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  data = {};
  c = [];

  constructor(private fetchBookingDetailsService: LabBookingService) {
  }

  ngOnInit() {
    this.getBookingDetails()
  }

  getBookingDetails() {
    console.log('rahul');

    let userId: string = localStorage.getItem('currentUser');
    this.fetchBookingDetailsService.getBookingDetails(userId)
      .subscribe(res => {
          console.log(res);

          this.c = [];
          this.data = res;

          let j = 0, l = 0;
          for (let i in this.data) {
            j++;
          }

          for (let k in res) {
            l++;
            if (l > j - 1) {
              break;
            }
            console.log(this.data[k]);
            this.c.push(this.data[k]);
          }
          console.log(JSON.parse(JSON.stringify(res)));
        },
        (err: HttpErrorResponse) => {
          console.log(err);
          console.log(err.error);
          console.log(err.name);
          console.log(err.message);
          console.log(err.status);
        },
        () => {
          console.log('complete');
        }
      );
  }
}
