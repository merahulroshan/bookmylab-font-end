import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {DashboardComponent} from './dashboard/dashboard.component';
import {SemBookingComponent} from './sem-booking/sem-booking.component';
import {SpmBookingComponent} from './spm-booking/spm-booking.component';
import {XrdBookingComponent} from './xrd-booking/xrd-booking.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {AuthGuard} from './auth.guard';
import {AuthService} from './auth.service';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {ContactUsComponent} from './contact-us/contact-us.component';
import {MyAccountComponent} from './my-account/my-account.component';
import {UserService} from './user.service';
import {BsDatepickerModule, TooltipModule} from "ngx-bootstrap";
import {NouisliderModule} from "ng2-nouislider";
import {BookingComponent} from './booking/booking.component';
import {LabBookingService} from "./services/labbooking.service";
import {PaymentComponent} from './payment/payment.component';
import {SlotbookingService} from "./services/slotbooking.service";
import {ForbiddenValidatorDirective} from "./validators/forbidden-name.directive";
import {PaymentService} from "./services/payment.service";
import { HeaderComponent } from './header/header.component';
import { VerifyComponent } from './verify/verify.component';
import { ManagerDashboardComponent } from './manager-dashboard/manager-dashboard.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SemBookingComponent,
    SpmBookingComponent,
    XrdBookingComponent,
    LoginComponent,
    RegisterComponent,
    ContactUsComponent,
    MyAccountComponent,
    BookingComponent,
    PaymentComponent,
    ForbiddenValidatorDirective,
    HeaderComponent,
    VerifyComponent,
    ManagerDashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    NouisliderModule,
    TooltipModule.forRoot(),
    BsDatepickerModule.forRoot()
  ],
  providers: [AuthGuard, AuthService, UserService, LabBookingService, SlotbookingService, PaymentService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
