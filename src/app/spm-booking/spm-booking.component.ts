import {Component, OnInit} from '@angular/core';
import {HttpErrorResponse} from "@angular/common/http";
import {NgForm} from "@angular/forms";
import {Router} from "@angular/router";
import {LabBookingService} from "../services/labbooking.service";
import {Sem} from "../model/sem";
import {Spm} from "../model/spm";

@Component({
  selector: 'app-spm-booking',
  templateUrl: './spm-booking.component.html',
  styleUrls: ['./spm-booking.component.css']
})
export class SpmBookingComponent {
  loading: boolean = false;
  model = {};
  slotDetails: number[];
  dateBooked: string;
  title = " Lab Reservation";

  constructor(private semBookingService: LabBookingService, private router: Router) {
    console.log('dateBooked:');
    console.log(this.dateBooked);
  }

  onSlotSelected(slot: number[]) {
    this.slotDetails = slot;
    console.log('slot details:' + this.slotDetails);
  }

  onDateSelected(date: string) {
    this.dateBooked = date;
  }

  onFormSubmit(semForm: NgForm) {
    let isBooked: boolean = false;
    let bookingId: string;
    let amount: number = 500;
    this.loading = true;

    let form = new Spm(
      this.dateBooked,
      this.slotDetails[0],
      this.slotDetails[1],
      semForm.controls['material'].value,
      semForm.controls['method_preparation'].value,
      semForm.controls['toxic'].value == true ? '1' : '0',
      semForm.controls['conducting'].value == true ? '1' : '0',
      semForm.controls['material_type'].value,
      semForm.controls['other_requirements'].value);

    this.semBookingService.createSpmBooking(form).subscribe(res => {
        this.loading = false;
        console.log("starting");
        console.log(res);
        isBooked = JSON.parse(JSON.stringify(res)).success;
        bookingId = JSON.parse(JSON.stringify(res)).spm_lab.booking_id;
        console.log('booking id :' + bookingId);
      },
      (err: HttpErrorResponse) => {
        this.loading = false;
        console.log(err);
        console.log(err.error);
        console.log(err.name);
        console.log(err.message);
        console.log(err.status);
      },
      () => {
        this.loading = false;
        console.log('completed');
        if (isBooked) {
          console.log("booked success");
          this.router.navigate(['/payment', {amount: amount, booking_id: bookingId}]);
        }
      }
    );
  }
}

