import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {
  url = 'https://iitrprlab.000webhostapp.com/login.php';

  constructor(private http: HttpClient) {
  }

  login(username: string, password: string) {

    const fd = new FormData();
    fd.append('email', username);
    fd.append('password', password);

    return this.http.post(this.url, fd);
  }

  logout() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('roleId');
    console.log('logout successful');
  }
}
