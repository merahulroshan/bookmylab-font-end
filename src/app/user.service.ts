import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from './model/user';

@Injectable()
export class UserService {
  url_register = 'https://iitrprlab.000webhostapp.com/register.php';
  url_verify = 'https://iitrprlab.000webhostapp.com/verify.php';

  constructor(private http: HttpClient) {
  }

  create(user: User) {
    var fd = new FormData();
    fd.append('first_name', user.firstname);
    fd.append('middle_name', user.middlename);
    fd.append('last_name', user.lastname);
    fd.append('gender', user.gender);
    fd.append('email', user.email);
    fd.append('password', user.password);
    fd.append('role_id', "4");
    return this.http.post(this.url_register, fd);
  }

  verify(email: string, ver_code: string) {
    const fd = new FormData();
    fd.append('email', email);
    fd.append('hash', ver_code);
    return this.http.post(this.url_verify, fd);
  }

}

